#ZADANIE 1

#zamieszczanie ciagu znakow
#ciag znakow zawsze zamieszczamy w cudzyslowiu
#np. 'Hello World'

x = 'Hello world'

print(x)
print(type(x)) #type wskazuje nam ze jest to klasa 'str'

#ZADANIE 2

#morzna rowniez dodawac ciagi znakow
y = '....::a1 ' + ' b1::....'
print(y)

#ZADANIE 3

#mozna rowniez mnozyc ciagi znakow np.1
x = 'Hello world ' #dodajac spacje na koniec mozna dodac odstep
                   #po wydrukowaniu

print(x * 2)

#przyklad 2

x = 'hello World '
x = x * 2
print(x)



