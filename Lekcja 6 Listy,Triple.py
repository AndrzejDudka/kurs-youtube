#ZADANIE 1
#liste mozna edytowac
produkty = ['mleko', 'ser','parowki'] #za pomoca nawiasu [] mozemy wprowadzac wartosci listy
print(type(produkty))
print(produkty)
print(produkty[0])#odmiennie do poprzedniego sposobu nie wybieramy litery bo wybieramy z listy
print()
print('Zadanie 2')

#ZADANIE 2

produkt = ['mleko', 'ser', 'parowki']
produkt.append('szynka')#za pomoca kropki odnosimy sie do klasy w tym przypadku 'list'
                        #za pomoca funkcji .append dodajemy obiekt do klasy
produkt.append('szynka')
print(produkt.count('szynka'))#za pomoca funkcji .count liczymy produkty w liscie
print(produkt)
#extend dodajemy kolejna liste do istniejecej
#index na jakiej pozycji wystepuje obiekt
#insert dodaje obiekt w konkretne miesjce listy
#pop usuwanie obiekty z listy po wyznaczeniu miejsca
#remove usowanie nie wedlug pozycji a wedlug nazwy
print()
print('Zadanie 2')

#ZADANIE 2
#Tuple nie mozna edytowac ale za to sa bardziej wydajne od listy

prod = ('jajka','maslo','kabanosy')
print(type(prod))
print(prod)
print()
print('Zadanie 3')

#ZADANIE 3
#Słowniki
#Dla slownikow urzywamy klamerek {}
        #klucz == wartosc
osoba ={'wiek':20 , 'imie': 'Janek','nazwisko':'Bold'}
print(osoba['wiek'])
print(osoba['nazwisko'])
print(osoba['imie'])

print(type(osoba))
#.keys tworzy liste kluczy
#.values tworzy liste wartosci

